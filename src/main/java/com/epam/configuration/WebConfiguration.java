package com.epam.configuration;

import com.epam.Interceptor.AdminInterceptor;
import com.epam.Interceptor.AuthInterceptor;
import com.epam.Interceptor.RootInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    private final AuthInterceptor authInterceptor;
    private final AdminInterceptor adminInterceptor;
    private final RootInterceptor rootInterceptor;

    @Autowired
    public WebConfiguration(AuthInterceptor authInterceptor, AdminInterceptor adminInterceptor, RootInterceptor rootInterceptor) {
        this.authInterceptor = authInterceptor;
        this.adminInterceptor = adminInterceptor;
        this.rootInterceptor = rootInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor).addPathPatterns("/**").excludePathPatterns("/authentication", "/registration");
        registry.addInterceptor(adminInterceptor).addPathPatterns("/admin");
        registry.addInterceptor(rootInterceptor).addPathPatterns("/root");
    }
}
