package com.epam.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class DBConfig {

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://dumbo.db.elephantsql.com:5432/klusozhi");
        dataSource.setUsername("klusozhi");
        dataSource.setPassword("ImeFPFC9OmaAn49znKmZUQ2iVjfa6su1");
        return dataSource;
    }
}
