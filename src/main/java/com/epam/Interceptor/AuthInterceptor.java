package com.epam.Interceptor;

import com.epam.dto.User;
import com.epam.service.SessionUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

@Component
public class AuthInterceptor implements HandlerInterceptor {

    private final SessionUserService sessionUserService;

    @Autowired
    public AuthInterceptor(SessionUserService sessionUserService) {
        this.sessionUserService = sessionUserService;
    }

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        User user = sessionUserService.getCurrentSessionUser();
        if (Objects.isNull(user)) {
            httpServletResponse.sendRedirect("/authentication");
            return false;
        }
        httpServletRequest.setAttribute("user", user);
        return true;
    }
}
