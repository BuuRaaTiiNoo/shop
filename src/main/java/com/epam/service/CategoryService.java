package com.epam.service;

import com.epam.dto.Category;
import com.epam.dto.Product;

import java.util.List;

public interface CategoryService {

    public void addCategory(Category category);

    public void deleteCategoryByName(String nameCategory);

    public void deleteCategoryById(long id);

    public Category getCategoryById(long id);

    public Category getCategoryByName(String name);

    public List<Category> getAllCategories();
}
