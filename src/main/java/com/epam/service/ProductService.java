package com.epam.service;

import com.epam.dto.Product;

import java.util.List;

public interface ProductService {

    public void addProduct(Product product);

    public Product getProductById(long id);

    public List<Product> getProductsByName(String name);

    public void deleteProductById(long id);

    public List<Product> getAllProducts();
}
