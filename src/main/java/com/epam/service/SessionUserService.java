package com.epam.service;

import com.epam.dto.User;

public interface SessionUserService {

    public void setCurrentSessionUser(User user);

    public User getCurrentSessionUser();
}
