package com.epam.service.serviceImpl;

import com.epam.dto.Product;
import com.epam.dto.Role;
import com.epam.dto.User;
import com.epam.repository.UserRepository;
import com.epam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void addUser(User user) {
        userRepository.addUser(user);
    }

    @Override
    public User getUserById(long id) {
        return userRepository.getUserById(id);
    }

    @Override
    public User getUserByLogin(String login) {
        return userRepository.getUserByLogin(login);
    }

    @Override
    public void updateUser(User user) {
        userRepository.updateUser(user);
    }

    @Override
    public void addProductIntoBasket(Product product) {
        userRepository.addProductIntoBasket(product);
    }

    @Override
    public void deleteProductFromBasket(Product product) {
        userRepository.deleteProductFromBasket(product);
    }

    @Override
    public User authenticateUser(User user) {
        User foundUser = userRepository.getUserByLogin(user.getLogin());
        if (foundUser != null && !foundUser.getPassword().equals(user.getPassword())) {
            return null;
        }
        return foundUser;
    }

    @Override
    public void changeAccess(User user){
        switch (user.getRole()) {
            case CLIENT:
                user.setRole(Role.ADMIN);
                break;
            case ADMIN:
                user.setRole(Role.CLIENT);
                break;
            case ROOT:
                break;
        }
        userRepository.updateUser(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }


}
