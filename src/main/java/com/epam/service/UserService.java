package com.epam.service;

import com.epam.dto.Product;
import com.epam.dto.User;

import java.util.List;

public interface UserService {
    public void addUser(User user);

    public User getUserById(long id);

    public User getUserByLogin(String login);

    public void updateUser(User user);

    public void addProductIntoBasket(Product product);

    public void deleteProductFromBasket(Product product);

    public void changeAccess(User user);

    public User authenticateUser(User user);

    public List<User> getAllUsers();
}
