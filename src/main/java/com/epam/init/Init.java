package com.epam.init;

import com.epam.dto.*;
import com.epam.repository.repositoryImpl.CategoryRepositoryImpl;
import com.epam.repository.repositoryImpl.ProductRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class Init {

    private final ProductRepositoryImpl productRepository;
    private final CategoryRepositoryImpl categoryRepository;

    @Autowired
    public Init(ProductRepositoryImpl productRepository, CategoryRepositoryImpl categoryRepository) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
    }

    @PostConstruct
    public void init() {
        Product product = new Product("qwe", "100", "description");
        product.setId(1L);
        productRepository.addProduct(product);
        productRepository.addProduct(new Product("asd", "120", "description"));
        productRepository.addProduct(new Product("vbn", "140", "description"));
        productRepository.addProduct(new Product("zxc", "160", "description"));
        productRepository.addProduct(new Product("rty", "180", "description"));
        productRepository.addProduct(new Product("fgh", "200", "description"));

        Category category = new Category("FirstCategory", productRepository.getAllProducts());
        category.setId(1L);
        categoryRepository.addCategory(category);
        categoryRepository.addCategory(new Category("SecondCategory", productRepository.getAllProducts()));
        categoryRepository.addCategory(new Category("ThirdCategory", productRepository.getAllProducts()));
        categoryRepository.addCategory(new Category("LastCategory", productRepository.getAllProducts()));
    }
}
