package com.epam.dto;

import javax.validation.constraints.NotNull;

public class Product implements Cloneable{
    private long id;
    @NotNull
    private String name;
    @NotNull
    private String cost;
    @NotNull
    private String description;

    public Product() {
    }

    public Product(String name, String cost, String description) {
        this.name = name;
        this.cost = cost;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cost='" + cost + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
