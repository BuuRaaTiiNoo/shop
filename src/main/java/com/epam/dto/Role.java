package com.epam.dto;

public enum Role {
    ROOT,
    ADMIN,
    CLIENT
}
