package com.epam.dto;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

public class Category {

    private long id;
    @NotEmpty
    private String name;
    private List<Product> products;

    public Category() {
        this.products = new ArrayList<>();
    }

    public Category(String name, List<Product> products) {
        this.name = name;
        this.products = products;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Product getProductFromCategoryByName(String name){
        return products.stream().filter(product -> product.getName().equals(name)).findFirst().orElse(null);
    }

    public Product getProductFromCategoryById(long id){
        return products.stream().filter(product -> product.getId() == id).findFirst().orElse(null);
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", products=" + products +
                '}';
    }
}
