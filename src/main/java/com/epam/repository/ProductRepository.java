package com.epam.repository;

import com.epam.dto.Product;

import java.util.List;

public interface ProductRepository {

    public void addProduct(Product product);

    public Product getProductById(long id);

    public List<Product> getProductsByName(String name);

    public void deleteProductById(long id);

    public List<Product> getAllProducts();
}
