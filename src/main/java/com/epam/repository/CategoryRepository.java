package com.epam.repository;

import com.epam.dto.Category;
import com.epam.dto.Product;

import java.util.List;

public interface CategoryRepository {

    public void addCategory(Category category);

    public void deleteCategoryByName(String nameCategory);

    public void deleteCategoryById(long id);

    public Category getCategoryById(long id);

    public Category getCategoryByName(String name);

    public void updateCategory(Category category);

    public List<Category> getAllCategories();
}
