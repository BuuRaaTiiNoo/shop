package com.epam.repository.repositoryImpl;

import com.epam.dto.Product;
import com.epam.repository.ProductRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ProductRepositoryImpl implements ProductRepository {

    private List<Product> products;

    public ProductRepositoryImpl() {
        this.products = new ArrayList<>();
    }

    @Override
    public void addProduct(Product product) {
        products.add(product);
    }

    @Override
    public Product getProductById(long id) {
        return products.stream().filter(product -> product.getId() == id).findFirst().get();
    }

    @Override
    public List<Product> getProductsByName(String name) {
        return products.stream()
                .filter(product -> product.getName().equals(name))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteProductById(long id) {
        products.stream()
                .filter(product -> product.getId() == id)
                .findFirst()
                .map(p -> {
                    products.remove(p);
                    return p;
                });
    }

    @Override
    public List<Product> getAllProducts() {
        return products;
    }
}
