package com.epam.repository.repositoryImpl;

import com.epam.dto.Category;
import com.epam.repository.CategoryRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

    private List<Category> categories;

    public CategoryRepositoryImpl() {
        this.categories = new ArrayList<>();
    }

    @Override
    public void addCategory(Category category) {
        categories.add(category);
    }

    @Override
    public void deleteCategoryByName(String nameCategory) {

    }

    @Override
    public void deleteCategoryById(long id) {

    }

    @Override
    public Category getCategoryById(long id) {
        return categories.stream().filter(category -> category.getId() == id).findFirst().orElse(null);
    }

    @Override
    public Category getCategoryByName(String name) {
        return categories.stream().filter(category -> category.getName().equals(name)).findFirst().orElse(null);
    }

    @Override
    public void updateCategory(Category category) {

    }

    @Override
    public List<Category> getAllCategories() {
        return categories;
    }
}
