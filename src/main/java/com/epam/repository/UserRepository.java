package com.epam.repository;

import com.epam.dto.Product;
import com.epam.dto.User;

import java.util.List;

public interface UserRepository {

    public void addUser(User user);

    public User getUserById(long id);

    public User getUserByLogin(String login);

    public void updateUser(User user);

    public void deleteUserById(long id);

    public void addProductIntoBasket(Product product);

    public List<User> getAllUsers();

    public void deleteProductFromBasket(Product product);
}
