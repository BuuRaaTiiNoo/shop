package com.epam.controller;

import com.epam.dto.Category;
import com.epam.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class MainController {

    private final CategoryService categoryService;

    @Autowired
    public MainController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping({"/", "/index"})
    public ModelAndView index(ModelAndView modelAndView) {
        List<Category> categories = categoryService.getAllCategories();
        modelAndView.setViewName("index");
        modelAndView.addObject("categories", categories);
        return modelAndView;
    }
}