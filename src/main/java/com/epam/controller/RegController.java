package com.epam.controller;

import com.epam.dto.User;
import com.epam.exception.UserAlreadyExistsException;
import com.epam.service.SessionUserService;
import com.epam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegController {

    private final SessionUserService sessionUserService;
    private final UserService userService;

    @Autowired
    public RegController(SessionUserService sessionUserService, UserService userService) {
        this.sessionUserService = sessionUserService;
        this.userService = userService;
    }

    @GetMapping("/registration")
    public ModelAndView authentication(ModelAndView modelAndView) {
        modelAndView.setViewName("registration");
        return modelAndView;
    }

    @PostMapping("/registration")
    public ModelAndView authentication(ModelAndView modelAndView, User user) throws UserAlreadyExistsException {
        for(User foundUser : userService.getAllUsers()){
            if (foundUser.getLogin().equals(user.getLogin()))
                throw new UserAlreadyExistsException("Пользователь с таким именем уже существует");
        }
        userService.addUser(user);
        sessionUserService.setCurrentSessionUser(user);
        modelAndView.setViewName("redirect:");
        return modelAndView;
    }
}
