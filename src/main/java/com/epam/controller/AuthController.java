package com.epam.controller;

import com.epam.dto.User;
import com.epam.exception.UserNotFoundException;
import com.epam.service.SessionUserService;
import com.epam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class AuthController {

    private final SessionUserService sessionUserService;
    private final UserService userService;

    @Autowired
    public AuthController(SessionUserService sessionUserService, UserService userService) {
        this.sessionUserService = sessionUserService;
        this.userService = userService;
    }

    @GetMapping("/authentication")
    public ModelAndView authentication(ModelAndView modelAndView) {
        modelAndView.setViewName("authentication");
        return modelAndView;
    }

    @PostMapping("/authentication")
    public ModelAndView authentication(ModelAndView modelAndView, User user) throws UserNotFoundException {
        User foundUser = userService.authenticateUser(user);
        if (foundUser != null) {
            sessionUserService.setCurrentSessionUser(foundUser);
            modelAndView.setViewName("redirect:");
        } else
            throw new UserNotFoundException("Неверный логин или пароль");
        return modelAndView;
    }

    @GetMapping("/logout")
    public ModelAndView logout(ModelAndView modelAndView, HttpServletRequest httpServletRequest) {
        httpServletRequest.getSession().invalidate();
        modelAndView.setViewName("redirect:authentication");
        return modelAndView;
    }
}
