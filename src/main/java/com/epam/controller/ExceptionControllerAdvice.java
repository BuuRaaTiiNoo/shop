package com.epam.controller;

import com.epam.exception.UserAlreadyExistsException;
import com.epam.exception.UserNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionControllerAdvice {
    @ExceptionHandler(UserNotFoundException.class)
    public ModelAndView userNotFoundException(UserNotFoundException e) {
        ModelAndView modelAndView = new ModelAndView("authentication");
        modelAndView.addObject("message", e.getMessage());
        return modelAndView;
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    public ModelAndView exception(UserAlreadyExistsException e) {
        ModelAndView modelAndView = new ModelAndView("registration");
        modelAndView.addObject("message", e.getMessage());
        return modelAndView;
    }
}
