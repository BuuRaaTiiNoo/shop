package com.epam.controller;

import com.epam.dto.Product;
import com.epam.dto.User;
import com.epam.service.ProductService;
import com.epam.service.SessionUserService;
import com.epam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class UserController {

    private final UserService userService;
    private final ProductService productService;
    private final SessionUserService sessionUserService;

    @Autowired
    public UserController(UserService userService, ProductService productService, SessionUserService sessionUserService) {
        this.userService = userService;
        this.productService = productService;
        this.sessionUserService = sessionUserService;
    }

    @GetMapping("basket")
    public ModelAndView basket(ModelAndView modelAndView) {
        User user = sessionUserService.getCurrentSessionUser();
        return modelAndView;
    }

    @PostMapping("basket/add/{productId}")
    public void addProductIntoBasket(@PathVariable("productId") long productId) {
        Product product = productService.getProductById(productId);
        userService.addProductIntoBasket(product);
    }

    @PostMapping("basket/delete/{productId}")
    public void deleteProductIntoBasket(@PathVariable("productId") long productId) {
        Product product = productService.getProductById(productId);
        userService.deleteProductFromBasket(product);
    }
}
