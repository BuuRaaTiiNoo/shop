package com.epam.controller;


import com.epam.dto.User;
import com.epam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class RootController {

    private final UserService userService;

    @Autowired
    public RootController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/root")
    public ModelAndView rootPanel(ModelAndView modelAndView) {
        List<User> users = userService.getAllUsers();
        modelAndView.addObject("users", users);
        modelAndView.setViewName("root");
        return modelAndView;
    }

    @PostMapping("root/{login}")
    public ModelAndView changeAccessRight(@PathVariable("login") String login, ModelAndView modelAndView) {
        User user = userService.getUserByLogin(login);
        userService.changeAccess(user);
        modelAndView.setViewName("redirect:/root");
        return modelAndView;
    }

}
