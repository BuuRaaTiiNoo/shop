package com.epam.controller;

import com.epam.dto.Category;
import com.epam.dto.Product;
import com.epam.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
public class AdminController {

    private final CategoryService categoryService;

    @Autowired
    public AdminController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/admin")
    public ModelAndView adminPanel(ModelAndView modelAndView) {
        List<Category> categories = categoryService.getAllCategories();
        modelAndView.addObject("categories", categories);
        modelAndView.setViewName("admin");
        return modelAndView;
    }

    @PostMapping("admin/delete/{categoryId}/{productId}")
    public ModelAndView deleteProduct(@PathVariable("categoryId") long categoryId, @PathVariable("productId") long productId, ModelAndView modelAndView) {

        Product findProduct = categoryService.getCategoryById(categoryId).getProductFromCategoryById(productId);
        categoryService.getCategoryById(categoryId).getProducts().stream()
                .filter(product -> product.equals(findProduct))
                .findFirst()
                .map(p -> {
                    categoryService.getCategoryById(categoryId).getProducts().remove(p);
                    return p;
                });
        modelAndView.setViewName("redirect:/admin");
        return modelAndView;
    }

    @PostMapping("/admin/{categoryId}")
    public ModelAndView addProduct(@PathVariable long categoryId, ModelAndView modelAndView, @Valid Product product, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            modelAndView.addObject("errors", bindingResult.getAllErrors());
            modelAndView.setViewName("redirect:/admin");
            return modelAndView;
        }
        categoryService.getCategoryById(categoryId).getProducts().add(product);
        modelAndView.setViewName("redirect:/admin");
        return modelAndView;
    }

    @PostMapping("admin/update/{productId}")
    public ModelAndView updateProduct(@PathVariable("productId") long productId, ModelAndView modelAndView, Product product) {
        product.setId(productId);
        for(Category category : categoryService.getAllCategories()){
            for (Product p : category.getProducts()){
                if (p.getId() == product.getId()){
                    p.setCost(product.getCost());
                    p.setDescription(product.getDescription());
                    p.setName(product.getName());
                }
            }
        }
        modelAndView.setViewName("redirect:/admin");
        return modelAndView;
    }
}
