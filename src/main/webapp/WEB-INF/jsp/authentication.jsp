<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<h3 class="text-center mt-5">Авторизация</h3>
    <div class="row p-3">
        <div class="col"></div>
        <div class="col-3 p-3">
            <div class="p-3">
                <form class="form-signin flogin" method="POST" action="authentication">
                    <input type="text" class="form-control mt-3 text-center" name="login" id="login" required placeholder="Логин" oninvalid="this.setCustomValidity('Поле логин не может быть пустым')"
                        oninput="this.setCustomValidity('')" />
                    <input type="password" class="form-control mt-3 text-center" name="password" id="password" required placeholder="Пароль"
                        oninvalid="this.setCustomValidity('Поле пароль не может быть пустым')" oninput="this.setCustomValidity('')"/>
                    <button class="btn btn-lg btn-primary btn-block mt-3 mb-5" type="submit">Вход</button>
                </form>
                <div class="message text-center">${message}</div>
                <div class="text-center mt-5">
                    <a href="registration" class="text-center mt-5">Зарегистрироваться</a>
                </div>
            </div>
        </div>
        <div class="col"></div>
    </div>
<%@include file="footer.jsp"%>