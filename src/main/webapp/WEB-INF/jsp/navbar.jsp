<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<nav class="navbar navbar-expand-sm navbar-light bg-light fixed-top">
        <a class="navbar-brand" href="/">Online Shop</a>
        <div class="navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <c:if test = "${user.role == 'ROOT'}">
                        <a class="nav-link" href="root">Root Panel</a>
                    </c:if>
                    <c:if test = "${user.role == 'ADMIN'}">
                        <a class="nav-link" href="admin">Admin Panel</a>
                    </c:if>
                </li>
            </ul>
            <button type="button" class="btn btn-light modal-search" data-toggle="modal" data-target="#search">
                Search
            </button>
            <button type="button" class="btn btn-light" id = "btn-buy" data-toggle="modal" data-target="#basket-modal">
                Buy
                <span class="badge badge-secondary"></span>
            </button>
            <a href="logout">
                <button type="button" class="btn btn-light login">${user.login}</button>
            </a>
        </div>
    </nav>