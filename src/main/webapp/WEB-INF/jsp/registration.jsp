<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<h3 class="text-center mt-5">Регистрация</h3>
    <div class="row p-3">
        <div class="col"></div>
        <div class="col-4 p-3">
            <div class="p-3">
                <form class="form-signin freg" name="freg" action="registration" method="POST">
                    <input type="text" class="form-control mt-1 text-center" name="firstName" id="firstName" placeholder="Имя" required oninvalid="this.setCustomValidity('Поле Имя не может быть пустым')"
                        oninput="this.setCustomValidity('')" />
                    <input type="text" class="form-control mt-1 text-center" name="lastName" id="lastName" placeholder="Фамилия" required oninvalid="this.setCustomValidity('Поле Фамилия не может быть пустым')"
                        oninput="this.setCustomValidity('')" />
                    <input type="text" class="form-control mt-1 text-center" name="login" id="login" placeholder="Логин" required oninvalid="this.setCustomValidity('Поле Логин не может быть пустым')"
                        oninput="this.setCustomValidity('')" />
                    <input type="password" class="form-control mt-1 text-center" name="password" id="password" placeholder="Пароль" required
                        oninvalid="this.setCustomValidity('Поле Пароль не может быть пустым')" oninput="this.setCustomValidity('')"
                    />
                    <input type="password" class="form-control mt-1 text-center" name="confpassword" id="confpassword" placeholder="Повторить пароль"
                        required/>
                    <div class="text-center mt-3">
                        <a href="#" class="text-center">Сообщить об ошибке</a>
                    </div>
                    <button class="btn btn-lg btn-primary btn-block mt-5 mb-5" id="btnreg" type="submit">Зарегистрироваться</button>
                </form>
                <div class="message text-center">${message}</div>
                <div class="text-center mt-5">
                    <a href="authentication" class="text-center mt-5">Авторизация</a>
                </div>
            </div>
        </div>
        <div class="col"></div>
    </div>
<%@include file="footer.jsp"%>
