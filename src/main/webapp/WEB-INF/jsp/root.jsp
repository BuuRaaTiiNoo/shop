<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<%@include file="navbar.jsp"%>

<div class="container text-center">
  <table class="table text-center">
    <thead class="thead-light">
      <tr>
        <th>Логин</th>
        <th>Текущие права</th>
        <th>Изменить права</th>
      </tr>
    </thead>
    <tbody>
        <c:forEach items="${users}" var="user">
            <tr>
                <td>${user.login}</td>
                <td>${user.role}</td>
                <td>
                    <form action="root/${user.login}" method="post">
                        <input id="changeAccess" type="submit" value="Изменить">
                    </form>
                </td>
            </tr>
        </c:forEach>
    </tbody>
  </table>
</div>

<%@include file="footer.jsp"%>