<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>

<%@include file="navbar.jsp"%>

    <div class="content">
        <c:forEach items="${categories}" var="category">
            <div class="category">
                <div class="sticky-top">
                    <h3>${category.name}</h3>
                </div>
                <div class="container-fluid section" id="section">
                    <div class="d-flex flex-wrap bg-default">
                        <c:forEach items="${category.products}" var="product">
                            <div class="p-1 border ml-3">
                                <div class="card">
                                    <div class="card-body prod">
                                        <div>
                                            <img src="https://placehold.it/150x80?text=IMAGE" class="img-responsive" alt="Image">
                                        </div>
                                        <div>
                                            <p class="card-text">${product.name}</p>
                                            <p class="card-text qwerty" style="display: none;">${product.description}</p>
                                        </div>
                                        <div class="description" style="display: none;">
                                            <div class="cost ml-3 mr-1" style="display: inline;">${product.cost}</div>
                                            <span class="mr-3">Rub</span>
                                            <button type="button" class="btn btn-default add-basket ml-3 mr-3" id = "${product.id}">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
    <footer class="text-muted mt-3 p-3 mb-3">
        <div class="container">
            <p>This is ONLINE SHOP!!!!!</p>
        </div>
     </footer>

    <div class="modal" id="basket-modal">
        <%@include file="basket.jsp"%>
    </div>

    <%@include file="search.jsp"%>
<%@include file="footer.jsp"%>

