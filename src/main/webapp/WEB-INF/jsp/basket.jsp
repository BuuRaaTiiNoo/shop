<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100">Корзина</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div id="content-basket">
                        <c:forEach items="${basket}" var="product">
                            <div class="card">
                                <div class="card-body">
                                    <img src="https://placehold.it/150x80?text=IMAGE" class="img-responsive" alt="Image" style="display: inline;">
                                    <div style="display: inline; position: absolute; margin-left:25%">
                                        <p class="card-text">${product.name}</p>
                                        <p class="card-text">${product.description}</p>
                                    </div>
                                    <div style = "display: inline; position: absolute; right: 10;">
                                        <div class="cost" style="display: inline;">${product.cost}</div>
                                        <span>Rub</span>
                                        <button type="button" class="btn btn-default del-product-from-basket ml-1" style="display: inline;" id="${product.id}">X</button>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                    <div class="row mt-1">
                        <div class="col-3">
                            Стоимость товаров
                        </div>
                        <div class="col-3">
                            <p class="sum"></p>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-3">
                            Скидка
                        </div>
                        <div class="col-3">
                            <p class="discount"></p>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-3">
                            Стоимость товаров со скидкой
                        </div>
                        <div class="col-3">
                            <p class="result"></p>
                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Оформить</button>
                </div>
            </div>
        </div>


