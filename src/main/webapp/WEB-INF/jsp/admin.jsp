<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<%@include file="navbar.jsp"%>

<div class="content">
    <c:forEach items="${categories}" var="category">
        <div class="category">
            <div>
                <h3>${category.name}</h3>
            </div>
            <table class="add-product table table-dark table-striped text-center">
                <tr>
                    <th>Product Name</th>
                    <th>Product Cost</th>
                    <th>Product Description</th>
                    <th>Add Product</th>
                </tr>
                <tr>
                    <form action="admin/${category.id}/" method="post">
                        <td>
                            <input type="text" class="form-control text-center" name="name" id="name" required placeholder="Название продукта" oninvalid="this.setCustomValidity('Поле не может быть пустым')"
                                oninput="this.setCustomValidity('')" />
                        </td>
                        <td>
                            <input type="text" class="form-control text-center" name="cost" id="cost" required placeholder="Цена" oninvalid="this.setCustomValidity('Поле не может быть пустым')"
                                oninput="this.setCustomValidity('')" />
                        </td>
                        <td>
                            <input type="text" class="form-control text-center" name="description" id="description" required placeholder="Описание продукта"
                                oninvalid="this.setCustomValidity('Поле не может быть пустым')" oninput="this.setCustomValidity('')"/>
                        </td>
                        <td>
                            <input id="changeProduct" type="submit" value="Добавить">
                        </td>
                    </form>
                </tr>
                <c:forEach items="${category.products}" var="product">
                    <tr>
                    <form action="admin/update/${product.id}" method="post">
                        <td><input type="text" class="form-control text-center" name="name" id="name" placeholder="Название продукта" value = "${product.name}"/></td>
                        <td><input type="text" class="form-control text-center" name="cost" id="cost" placeholder="Цена" value = "${product.cost}"/></td>
                        <td><input type="text" class="form-control text-center" name="description" id="description"  placeholder="Описание продукта" value = "${product.description}"/></td>
                        <td>
                            <input id="update-product" type="submit" value="Изменить">
                        </td>
                    </form>
                        <td>
                            <form action="admin/delete/${category.id}/${product.id}/" method="post">
                                <input id="delete-product" type="submit" value="X">
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </c:forEach>
</div>
<div class="modal" id="basket-modal">
    <%@include file="basket.jsp"%>
</div>
<%@include file="search.jsp"%>
<%@include file="footer.jsp"%>
