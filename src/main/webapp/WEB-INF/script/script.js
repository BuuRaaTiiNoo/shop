$(document).ready(function () {
    const value = $(".p-1").css("width");
    //search
    let isResizeble = false;
    let val = null;
    $("#searchbtn").click(function () {
        let product;
        let content = $(".bodycontent").children().children();
        val = $("#searchform").val().toLowerCase();

        if (!isResizeble) {
            $(".content .p-1").each(function () {
                product = $(this);
                content.append(product.html());
            });
            isResizeble = true;
        }
        $(".bodycontent").find(".card-body").removeClass("d-flex justify-content-between bg-default mb-3");
        $(".bodycontent").find(".card-body").children().removeClass("p-2 flex-fill bg-default");
        $(".bodycontent").find(".card-body").children().css({ "display": "block" });
        $(".bodycontent").find(".description").css("display", "none");
        $(".bodycontent .p-1").css({ "width": value });

        $(".bodycontent .card-text").filter(
            function () {
                $(this.parentNode.parentNode.parentNode).toggle($(this.parentNode.parentNode.parentNode).text().toLowerCase().indexOf(val) > -1)
            })
    });

    //collapse
    $(".prod").click(function () {
        //Устанавливаем ширину 100%
        $(this.parentNode.parentNode).css({ "width": "100%" });

        //Отображаем скрытое поле description, выстраиваем элементы.
        $(this).children().css({ "display": "inline" });
        $(this).children().children().css({ "display": "inline" });
        $(this).children().children(".qwerty").css({ "display": "block" });

        //Добавление классов к элементам
        $(this).addClass("d-flex justify-content-between bg-default mb-3");
        $(this).children().addClass("p-2 flex-fill bg-default");

        //Позиционирование элементов
        $(this).children().css({ "text-align": "center" });
        $(this).children().first().css({ "text-align": "left" });
        $(this).children().last().css({ "text-align": "right" });

        //Удаляем классы
        $(".category .prod").not(this).children().parent().removeClass("d-flex justify-content-between bg-default mb-3");
        $(".category .prod").not(this).children().removeClass("p-2 flex-fill bg-default");

        //Восстанавливаем значения полей
        $(".category .prod").not(this).children().children().css({ "display": "block" });
        $(".category .prod").not(this).children(".description").css("display", "none");
        $(".category .prod").not(this).children().children(".qwerty").css({ "display": "none" });

        $(".category .p-1").not(this.parentNode.parentNode).css({ "width": value });
    });

    //view basket
    $("#btn-buy").click(function () {
            $.get("/basket", function(data){
                $("#basket-modal").html(data);
            });
    });

    //add into basket
    $(".add-basket").click(function () {
        $.post(
            "/basket/add/" + $(this).attr('id'),
            $(this).attr('id')
        ).always(function () {
            $("#basket-modal").html(data);
         });
    });

    //remove out of basket
    $("#basket-modal").on("click", ".del-product-from-basket", function () {
        $.post("/basket/delete/" + $(this).attr('id'))
            .always(function () {
                $.get("/basket", function(data){
                    $("#basket-modal").html(data);
                });
            });
    });
});