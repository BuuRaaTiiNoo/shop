<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
$(document).ready(
        function () {
            $.ajax({
                url: "http://localhost:8080/json"
            })
                .then(function (data) {
                    $.each(data, function (i, category) {
                        $(".category").append('<div style="margin-top: 50px; margin-bottom: 50px;"><h3 class="category-name">'+category.name+'</h3></div><table class="products'+i+' table table-striped text-center"><tr><th>Product Name</th><th>Product Cost</th><th>Product Description</th><th>Change Product</th></tr></table>');
                        $.each(category.products, function (j, product) {
                            $(".products"+i).append(
                            '<tr><td>'+product.name+'</td><td>'+product.cost+'</td><td>'+product.description+'</td><td><form action="admin/'+category.name+'/'+product.name+'/" method="post"><input id="delete-product" type="submit" value="X"></form></td></tr>'
                            );
                        });
                        $(".category").append(
                            '<form action="admin/'+category.name+'" method="post" id="add-product"><div class="input-group"><input type="text" class="form-control text-center" name="name" id="name"/><input type="text" class="form-control text-center" name="cost" id="cost"/><input type="text" class="form-control text-center" name="description" id="description" /><button class="btn btn-default w-25" type="submit">Добавить</button></div> </form>'
                        );
                    });
                })
        }
    );z