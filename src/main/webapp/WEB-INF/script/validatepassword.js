<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
let password = document.forms["freg"]["password"];
let confpassword = document.forms["freg"]["confpassword"];
function validatePassword() {
    if (confpassword.value === "") {
        confpassword.setCustomValidity("Поле не может быть пустым");
    }
    else
        if (password.value != confpassword.value) {
            confpassword.setCustomValidity("Пароли не совпадают");
        } else {
            confpassword.setCustomValidity('');
        }
}
password.onchange = validatePassword;
confpassword.onkeyup = validatePassword;